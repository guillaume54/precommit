;;; precommit.el --- Validate/reformat buffers using the "precommit" tool

;; Copyright (C) 2018-2019 Artem Malyshev
;; Copyright (C) 2022 Guillaume Pasquet 

;; Author: Guillaume Pasquet <dev@etenil.net>
;; Homepage: https://github.com/etenil/precommit
;; Version: 0.0.1
;; Package-Requires: ((emacs "26"))
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation; either version 3, or (at your
;; option) any later version.
;;
;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Precommit uses pre-commit to validate and format a buffer.  It can be
;; called explicitly on a certain buffer, but more conveniently, a
;; minor-mode 'precommit-mode' is provided that turns on automatically
;; running pre-commit on a buffer after saving.
;;
;; Installation:
;;
;; Add precommit.el to your load-path.
;;
;; To automatically format all Python buffers after saving, add the
;; function precommit-mode to your major mode hook:
;;
;; (add-hook 'python-mode-hook 'precommit-mode)
;;
;;; Code:

(require 'cl-lib)
(require 'chistory)

(defgroup precommit nil
  "Reformat Python code with \"black\".")

(defcustom precommit-executable "pre-commit"
  "Name of the executable to run."
  :type 'string)

(defcustom precommit-pop-error-buffer t
  "Always display the pre-commit buffer on error"
  :type 'boolean
  :safe #'booleanp)

(defun precommit-call-bin (file-path output-buffer)
  "Call process pre-commit.

Send INPUT-BUFFER content to the process stdin.  Saving the
output to OUTPUT-BUFFER.  Saving process stderr to OUTPUT-BUFFER.
Return pre-commit process the exit code."
  (let ((process (make-process :name "precommit"
                               :command `(,precommit-executable ,@(precommit-call-args file-path))
                               :buffer output-buffer
                               :sentinel (lambda (process event))
                               :noquery t)))

    (accept-process-output process nil nil t)
    (while (process-live-p process)
      (accept-process-output process nil nil t))
    (process-exit-status process)))

(defun precommit-call-args (file-path)
  "Build pre-commit process call arguments."
  (list "run" "--files" file-path))

;;;###autoload
(defun precommit-buffer (&optional display)
  "Run pre-commit in the path of the current buffer.

Show precommit output, if pre-commit exit abnormally and DISPLAY is t."
  (interactive (list t))
  (let* ((original-buffer (current-buffer))
         (tmpbuf (get-buffer-create "*precommit*")))
    ;; This buffer can be left after previous black invocation.  It
    ;; can contain error message of the previous run.
    (dolist (buf (list tmpbuf))
      (with-current-buffer buf
        (fundamental-mode)
        (read-only-mode -1)
        (erase-buffer)))
    
    (condition-case err
        (if (not (zerop (precommit-call-bin (buffer-file-name original-buffer) tmpbuf)))
            (error "Pre-commit failed, see %s buffer for details" (buffer-name tmpbuf))
          (mapc 'kill-buffer (list tmpbuf)))
      (error (message "%s" (error-message-string err))
             (when (or display precommit-pop-error-buffer)
               (with-selected-window (selected-window)
                 (with-current-buffer tmpbuf
                   (setq-local scroll-conservatively 0)
                   (command-history-mode))
                 (pop-to-buffer tmpbuf)))))
    (with-current-buffer original-buffer
      (revert-buffer :ignore-auto :noconfirm)))) ; Refresh for any formatting changes


;;;###autoload
(define-minor-mode precommit-mode
  "Automatically run pre-commit after saving."
  :lighter " pre-commit"
  (if precommit-mode
      (add-hook 'after-save-hook #'precommit-buffer nil 'local)
    (remove-hook 'after-save-hook #'precommit-buffer t)))


(provide 'precommit)

;;; precommit.el ends here
