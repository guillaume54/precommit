# Precommit

Use the `pre-commit` utility to validate/reformat your buffers on save.

## Usage

The whole buffer can be validate/formatted with `precommit-buffer`. If you want to format every time you save, enable `precommit-mode` in relevant buffers.

## Customization

There is currently no customisation.

## Lineage

This project is a fork from the original
[Blacken](https://github.com/pythonic-emacs/blacken) tool.

## License

`precommit.el` is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

SPDX-License-Identifier: GPL-3.0-or-later
